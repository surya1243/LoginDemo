package com.surya.loginTest.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCursor;
import com.surya.loginTest.repository.PlantRepository;

@Controller
@RestController
public class PieChartController {

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	PlantRepository plantRepo;

	@RequestMapping(value = "/count/{fieldName}", method = RequestMethod.GET)
	public Map<String, Long> getUniqueGenericClassesWithCount(@PathVariable String fieldName) {
		Map map = new HashMap<String, Integer>();
		DistinctIterable<String> coll = mongoTemplate.getCollection("plant").distinct(fieldName, String.class);
		MongoCursor cursor = coll.iterator();
		while (cursor.hasNext()) {
			String field = (String) cursor.next();
			map.put(field, plantRepo.countOrderOf(fieldName, field));
		}
		return map;
	}

}
