package com.surya.loginTest.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.opencv.core.Mat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import com.surya.imgprocess.util.BestMatching;
import com.surya.imgprocess.util.DifferenceFromAverage;
import com.surya.imgprocess.util.ImageFetcher;
import com.surya.loginTest.config.CloudinaryConfig;
import com.surya.loginTest.model.Plant;
import com.surya.loginTest.model.Similarity;
import com.surya.loginTest.repository.PlantRepository;

@Controller
public class WebUserController {
	ImageFetcher fetcher = new ImageFetcher();
	@Autowired
	private PlantRepository repository;

	@Autowired
	CloudinaryConfig cloudc;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showHomePage(Model model) {
		return "webusers/homepage";
	}

	@RequestMapping(value = "/leafdatabase", method = RequestMethod.GET)
	public String showLeafDataBase(Model model) {
		List<Plant> plantList = repository.findAll();
		model.addAttribute("plantList", plantList);
		return "webusers/leafdatabase";
	}

	@RequestMapping(value = "/singleview/{id}", method = RequestMethod.GET)
	public String getPlant(@PathVariable String id, Model model) {
		Optional<Plant> selectedPlant = repository.findById(id);
		model.addAttribute("plantDetails", selectedPlant.get());
		return "webusers/singleview";
	}

	@PostMapping(value = "/check")
	public String showMatchPlant(@RequestParam("file") MultipartFile file, Model model, HttpSession session) {
		List<Similarity> matchingleaf = new LinkedList<>();

		if (!((file == null) || (file.isEmpty()))) {
			try {
				Map result = saveImageinFile(file);
				model.addAttribute("remoteUploadUrl", result.get("url"));
				session.setAttribute("remoteUploadUrl", result.get("url"));
				// fetch image and extract feature
				Mat imgMat = fetcher.getMatrixFromImage(result.get("url").toString());
				DifferenceFromAverage diff = new DifferenceFromAverage(imgMat);
				List<Double> diffValues = diff.getDistanceDifferenceFromMean();

				// System.out.println("Size of contour points is "+diffValues);

				BestMatching bm = new BestMatching(diffValues, repository.findAll());
				matchingleaf = bm.getMatchingValues();

			} catch (FileNotFoundException e) {
				System.out.println("File not found.");
			}
		} else {
			System.out.println("file is empty SORRY.");
		}

		model.addAttribute("similarity", matchingleaf);
		session.setAttribute("latest", matchingleaf);
		return "redirect:/showmatch";
	}

	@RequestMapping(value = "/showmatch", method = RequestMethod.GET)
	public String showPlant(HttpSession session, Model model) {

		if (session != null) {
			model.addAttribute("remoteUploadUrl", session.getAttribute("remoteUploadUrl"));
			model.addAttribute("similarity", session.getAttribute("latest"));
			return "webusers/showmatch";
		}
		return "webusers/homepage";
	}

	@RequestMapping(value = "/singleview", method = RequestMethod.GET)
	public String showSinglePlant() {
		return "webusers/singleview";
	}

	// If flag is true image is uploaded to upload directory else to test directory
	public Map<String, String> saveImageinFile(MultipartFile file) {
		Map<String, String> result = new HashMap<String, String>();
		try {
			Map options = new HashMap();
			options.put("resourcetype", "auto");
			options.put("use_filename", true);
			options.put("folder", "test");
			Map uploadResult = cloudc.upload(file.getBytes(), options);
			result.put("url", uploadResult.get("url").toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

}
