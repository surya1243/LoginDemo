package com.surya.loginTest.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.opencv.core.Mat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.surya.imgprocess.util.DifferenceFromAverage;
import com.surya.imgprocess.util.ImageFetcher;
import com.surya.loginTest.config.CloudinaryConfig;
import com.surya.loginTest.model.Plant;
import com.surya.loginTest.repository.PlantRepository;

@Controller
public class LoginController {

	ImageFetcher fetcher = new ImageFetcher();
	@Autowired
	private PlantRepository repository;

	@Autowired
	CloudinaryConfig cloudc;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getLoginForm(HttpServletRequest request) {
		if (request.getRemoteUser() != null) {
			return "redirect:/dashboard";
		}
		return "login";
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String getDashboard(HttpServletRequest request, Model model) {

		return "index";
	}

	@RequestMapping(value = "/plant", method = RequestMethod.GET)
	public String gethomepage(Model model) {
		List<Plant> plantList = repository.findAll();
		model.addAttribute("count", repository.count());
		model.addAttribute("plantList", plantList);
		return "viewallplant";
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@GetMapping("/form")
	public String userAddPage(Model model) {
		model.addAttribute("count", repository.count());
		model.addAttribute("addStatus", true);

		return "plantform";
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@RequestMapping(value = "/viewplant", method = RequestMethod.GET)
	public String getPlantList(Model model) {
		List<Plant> plantList = repository.findAll();
		model.addAttribute("count", repository.count());
		System.out.println(plantList);
		model.addAttribute("plantList", plantList);
		return "viewallplant";
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@RequestMapping(value = "/viewplant/{id}", method = RequestMethod.GET)
	public String getPlant(@PathVariable String id, Model model) {
		Optional<Plant> selectedPlant = repository.findById(id);
		model.addAttribute("count", repository.count());
		model.addAttribute("plantDetails", selectedPlant.get());
		return "plantdetails";
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@GetMapping("/update/{id}")
	public String editPlant(Model model, @PathVariable String id) {
		System.out.println("in console Id: " + id);
		Optional<Plant> plant = repository.findById(id);
		model.addAttribute("count", repository.count());
		model.addAttribute("plant", plant.get());
		// model.addAttribute("addStatus", false);
		return "updateplant";
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@RequestMapping(value = "/plant/delete", method = RequestMethod.DELETE)
	public String deletePlant(@RequestParam("id") String id) {
		repository.deleteById(id);
		return "redirect:/viewplant";
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@SuppressWarnings("null")
	@PostMapping("/update")
	public String updatePlant(@Valid @ModelAttribute("plant") Plant plant,
			@RequestParam(value = "file", required = false) MultipartFile file,
			@RequestParam(value = "gallery", required = false) MultipartFile[] gallery) {

		if ((gallery == null) || (gallery.length == 0)) {

		} else {
			System.out.println("size of gallery " + gallery.length);
			String[] prevAlbums = repository.findById(plant.getId()).get().getAlbums();

			System.out.println("previous album length" + prevAlbums.length);
			List<String> newAlbums = saveMultipartArrayToServerGallery(gallery);

			for (int i = 0; i < prevAlbums.length; i++) {
				newAlbums.add(prevAlbums[i]);
			}
			// newAlbums.addAll(Arrays.asList());
			// clear null values
			newAlbums.removeAll(Collections.singleton(null));
			plant.setAlbums(newAlbums);
		}

		if (!((file == null) || (file.isEmpty()))) {
			try {
				Map savedFileName = saveImageinFile(file);
				plant.setImageUrl(savedFileName.get("url").toString());
				// fetch image and extract feature
				Mat imgMat = fetcher.getMatrixFromImage(plant.getImageUrl());
				DifferenceFromAverage diff = new DifferenceFromAverage(imgMat);
				List<Double> diffValues = diff.getDistanceDifferenceFromMean();

				System.out.println("Size of contour points is " + diffValues);
				plant.setDistanceDifferenceFromMean(diffValues);

				/// saving plant to database
			} catch (FileNotFoundException e) {
				System.out.println("File not found.");
			}
		}

		repository.save(plant);
		System.out.println("Added new Plant details with id : " + plant.getId());
		return "redirect:/viewplant";
	}

	public List<String> saveMultipartArrayToServerGallery(MultipartFile[] gallery) {
		List<String> urlList = new ArrayList<String>();
		for (int i = 0; i < gallery.length; i++) {
			urlList.add(saveMultipartToServerGallery(gallery[i]));
		}

		return urlList;
	}

	public String saveMultipartToServerGallery(MultipartFile singlePic) {
		if ((singlePic != null) && (!singlePic.isEmpty())) {
			return saveSingleImageToCloud(singlePic);
		} else
			return null;
	}

	/* This method saves image in file and return url of image */
	private String saveSingleImageToCloud(MultipartFile file) {
		String url = null;

		try {
			Map options = new HashMap();
			options.put("resourcetype", "auto");
			options.put("use_filename", true);
			options.put("folder", "upload-dir");
			Map uploadResult = cloudc.upload(file.getBytes(), options);
			url = new String(uploadResult.get("url").toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return url;
	}

	public Map<String, String> saveImageinFile(MultipartFile file) {
		Map<String, String> result = new HashMap<String, String>();
		try {
			Map options = new HashMap();
			options.put("resourcetype", "auto");
			options.put("use_filename", true);
			options.put("folder", "upload-dir");
			Map uploadResult = cloudc.upload(file.getBytes(), options);
			result.put("url", uploadResult.get("url").toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	@PreAuthorize("hasAuthority('ADMIN')")
	@DeleteMapping("/delete/{id}/{index}")
	public String deleteSinglePicFromGallery(@PathVariable String id, @PathVariable int index) {
		Optional<Plant> plant = repository.findById(id);
		List<String> list = Arrays.asList(plant.get().getAlbums());
		List<String> gallery = new LinkedList<String>(list);

		// remove null values
		gallery.removeAll(Collections.singleton(null));
		if (gallery.size() > index) {
			gallery.remove(index);
		}
		plant.get().setAlbums(gallery);
		System.out.println("deleted and saved:" + plant.get());
		repository.save(plant.get());
		return "redirect:/update/" + plant.get().getId();
	}
}
