package com.surya.loginTest.resource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.opencv.core.Mat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCursor;
import com.surya.imgprocess.util.BestMatching;
import com.surya.imgprocess.util.DifferenceFromAverage;
import com.surya.imgprocess.util.ImageFetcher;
import com.surya.loginTest.config.CloudinaryConfig;
import com.surya.loginTest.model.Plant;
import com.surya.loginTest.model.Similarity;
import com.surya.loginTest.repository.PlantRepository;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;

@RestController
@RequestMapping("/api")
public class PlantController {

	ImageFetcher fetcher = new ImageFetcher();

	@Autowired
	CloudinaryConfig cloudc;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	private PlantRepository repository;

	@SuppressWarnings("unchecked")
	@GetMapping(value = "/count/{fieldName}")
	public Map<String, Long> getUniqueGenericClassesWithCount(@PathVariable String fieldName) {
		Map map = new HashMap<String, Integer>();
		DistinctIterable<String> coll = mongoTemplate.getCollection("plant").distinct(fieldName, String.class);
		MongoCursor<String> cursor = coll.iterator();
		while (cursor.hasNext()) {
			String field = (String) cursor.next();
			map.put(field, repository.countOrderOf(fieldName, field));
		}
		return map;
	}

	@GetMapping("/find")
	public List<Plant> getAllPlants() {
		return repository.findRequiredInformationOnly();
	}

	@GetMapping("/find/{id}")
	public Optional<Plant> getPlant(@PathVariable String id) {
		return repository.findByIdAndNeglectDistanceVector(id);
	}

	@PostMapping(value = "/check")
	public List<Similarity> showMatchPlant(@RequestParam("file") MultipartFile file, Model model) {
		List<Similarity> matchingleaf = new LinkedList<>();

		if (!((file == null) || (file.isEmpty()))) {
			try {

				Map result = saveImageinFile(file);
				// model.addAttribute("remoteUploadUrl", result.get("url"));
				// fetch image and extract feature
				Mat imgMat = fetcher.getMatrixFromImage(result.get("url").toString());
				DifferenceFromAverage diff = new DifferenceFromAverage(imgMat);
				List<Double> diffValues = diff.getDistanceDifferenceFromMean();

				// System.out.println("Size of contour points is " + diffValues);

				BestMatching bm = new BestMatching(diffValues, repository.findAll());
				matchingleaf = bm.getMatchingValues();

			} catch (FileNotFoundException e) {
				System.out.println("File not found.");
			}
		} else {
			System.out.println("file is empty SORRY.");
		}

		return matchingleaf;
	}

	public Map<String, String> saveImageinFile(MultipartFile file) {
		Map<String, String> result = new HashMap<String, String>();
		try {
			Map options = new HashMap();
			options.put("resourcetype", "auto");
			options.put("use_filename", true);
			options.put("folder", "test");
			Map uploadResult = cloudc.upload(file.getBytes(), options);
			result.put("url", uploadResult.get("url").toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

}
