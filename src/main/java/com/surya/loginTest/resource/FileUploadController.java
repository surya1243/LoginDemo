package com.surya.loginTest.resource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.apache.commons.lang.RandomStringUtils;
import org.opencv.core.Mat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import com.surya.imgprocess.util.DifferenceFromAverage;
import com.surya.imgprocess.util.ImageFetcher;
import com.surya.loginTest.config.CloudinaryConfig;
import com.surya.loginTest.model.Plant;

import com.surya.loginTest.repository.PlantRepository;

@Controller
public class FileUploadController {

	public static final int ID_LENGTH = 10;

	ImageFetcher fetcher = new ImageFetcher();

	@Autowired
	private PlantRepository repo;

	@Autowired
	CloudinaryConfig cloudc;

	@PostMapping("/file/upload")
	public String submit(@Valid @ModelAttribute("plant") Plant plant, @RequestParam MultipartFile file, ModelMap model,
			@RequestParam MultipartFile[] gallery) {

		if (gallery == null) {
			System.out.println("gallery is null");
		} else {
			System.out.println("size of gallery " + gallery.length);
			List<String> savedGallery = saveMultipartArrayToServerGallery(gallery);
			// clear null values
			savedGallery.removeAll(Collections.singleton(null));
			plant.setAlbums(savedGallery);
		}

		plant.setId(generateUniqueId());
		if (file == null || file.isEmpty())
			System.out.println("file is empty");

		Map result = saveImageinFile(file);
		plant.setImageUrl(result.get("url").toString());

		try {
			// fetch image and extract feature
			Mat imgMat = fetcher.getMatrixFromImage(plant.getImageUrl());

			DifferenceFromAverage diff = new DifferenceFromAverage(imgMat);

			List<Double> diffValues = diff.getDistanceDifferenceFromMean();
			System.out.println("Size of contour points is " + diffValues);
			plant.setDistanceDifferenceFromMean(diffValues);

			/// saving plant to database
		} catch (FileNotFoundException e) {
			System.out.println("File not found at" + plant.getImageUrl());
		}

		repo.save(plant);

		return "redirect:/viewplant";
	}

	public String generateUniqueId() {
		return RandomStringUtils.randomAlphanumeric(ID_LENGTH);
	}

	/* This method saves image in file and return url of image */
	public Map<String, String> saveImageinFile(MultipartFile file) {
		Map<String, String> result = new HashMap<String, String>();
		try {
			Map options = new HashMap();
			options.put("resourcetype", "auto");
			options.put("use_filename", true);
			options.put("folder", "upload-dir");
			Map uploadResult = cloudc.upload(file.getBytes(), options);
			result.put("url", uploadResult.get("url").toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		// result.put("local", storageService.store(file));
		return result;
	}

	public List<String> saveMultipartArrayToServerGallery(MultipartFile[] gallery) {
		List<String> urlList = new ArrayList<String>();
		for (int i = 0; i < gallery.length; i++) {
			urlList.add(saveMultipartToServerGallery(gallery[i]));
		}

		return urlList;
	}

	public String saveMultipartToServerGallery(MultipartFile singlePic) {
		if ((singlePic != null) && (!singlePic.isEmpty())) {
			return saveSingleImageToCloud(singlePic);
		} else
			return null;
	}

	/* This method saves image in file and return url of image */
	private String saveSingleImageToCloud(MultipartFile file) {
		String url = null;

		try {
			Map options = new HashMap();
			options.put("resourcetype", "auto");
			options.put("use_filename", true);
			options.put("folder", "upload-dir");
			Map uploadResult = cloudc.upload(file.getBytes(), options);
			url = new String(uploadResult.get("url").toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return url;
	}
}