package com.surya.loginTest.model;

public class Similarity {
	private double similar;
	private Plant plant;

	public double getSimilar() {
		return similar;
	}

	public void setSimilar(double similar) {
		this.similar = similar;
	}

	public Plant getPlant() {
		return plant;
	}

	public void setPlant(Plant plant) {
		plant.setDistanceDifferenceFromMean(null);
		this.plant = plant;

	}
}
