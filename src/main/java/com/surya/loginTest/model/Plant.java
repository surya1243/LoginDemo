package com.surya.loginTest.model;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

@Document(collection = "plant")

public class Plant {

	@Id
	private String id;

	private String imageUrl;
	private String kingdom;
	private String subKingdom;
	private String superDivision;
	private String division;
	private String phylum;
	private String className;
	private String subClass;
	private String order;
	private String family;
	private String genus;
	private String species;
	private String commonName;
	private String description;
	private String wikiLink;
	private String[] albums;

	private double[] distanceDifferenceFromMean;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getKingdom() {
		return kingdom;
	}

	public void setKingdom(String kingdom) {
		this.kingdom = kingdom;
	}

	public String getSubKingdom() {
		return subKingdom;
	}

	public void setSubKingdom(String subKingdom) {
		this.subKingdom = subKingdom;
	}

	public String getSuperDivision() {
		return superDivision;
	}

	public void setSuperDivision(String superDivision) {
		this.superDivision = superDivision;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getPhylum() {
		return phylum;
	}

	public void setPhylum(String phylum) {
		this.phylum = phylum;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getSubClass() {
		return subClass;
	}

	public void setSubClass(String subClass) {
		this.subClass = subClass;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getGenus() {
		return genus;
	}

	public void setGenus(String genus) {
		this.genus = genus;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWikiLink() {
		return wikiLink;
	}

	public void setWikiLink(String wikiLink) {
		this.wikiLink = wikiLink;
	}

	public List<Double> getDistanceDifferenceFromMean() {
		if ((this.distanceDifferenceFromMean == null) || (this.distanceDifferenceFromMean.length == 0))
			return null;
		List<Double> returnList = new LinkedList<Double>();
		for (int i = 0; i < this.distanceDifferenceFromMean.length; i++)
			returnList.add(new Double(this.distanceDifferenceFromMean[i]));
		return returnList;
	}

	public void setDistanceDifferenceFromMean(List<Double> distanceDifferenceFromMean) {
		if (distanceDifferenceFromMean == null) {
			this.distanceDifferenceFromMean = null;
			return;
		}
		this.distanceDifferenceFromMean = new double[distanceDifferenceFromMean.size()];
		for (int i = 0; i < this.distanceDifferenceFromMean.length; i++) {
			this.distanceDifferenceFromMean[i] = distanceDifferenceFromMean.get(i).doubleValue(); // java 1.4 style
		}
	}

	public String[] getAlbums() {
		return albums;
	}

	public void setAlbums(String[] albums) {
		this.albums = albums;
	}

	public void setAlbums(List<String> albums) {
		this.albums = new String[albums.size()];
		this.albums = albums.toArray(this.albums);
		System.out.println(Arrays.toString(this.albums));
	}

	/*
	 * public List<String> getGalleryAsList() { return new
	 * LinkedList<String>(Arrays.asList(this.albums)); }
	 */
	@Override
	public String toString() {
		return this.commonName + " " + this.className + " diffFromMean" + this.distanceDifferenceFromMean;
	}

}
