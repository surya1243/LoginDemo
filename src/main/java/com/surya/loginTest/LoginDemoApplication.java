package com.surya.loginTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;

@SpringBootApplication
@EnableAutoConfiguration(exclude = { ErrorMvcAutoConfiguration.class })
public class LoginDemoApplication {

	public static void main(String[] args) {
		// Load library
		nu.pattern.OpenCV.loadShared();
		SpringApplication.run(LoginDemoApplication.class, args);
	}

}
