package com.surya.loginTest.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.surya.loginTest.model.Plant;

public interface PlantRepository extends MongoRepository<Plant, String> {
	@Query(value = "{}", fields = "{distanceDifferenceFromMean : 0}")
	List<Plant> findRequiredInformationOnly();

	@Query(value = "{ '_id' : ?0 }", fields = "{distanceDifferenceFromMean : 0}")
	Optional<Plant> findByIdAndNeglectDistanceVector(String id);

	@Query(value = "{?0: ?1}", count = true)
	public Long countOrderOf(String field, String value);

}
