package com.surya.imgprocess.exception;

public class NotSameSizeException extends Exception {
	public  NotSameSizeException(String message) {
		super(message);
	}
}
