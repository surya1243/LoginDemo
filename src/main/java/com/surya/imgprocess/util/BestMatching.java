package com.surya.imgprocess.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.surya.imgprocess.exception.NotSameSizeException;
import com.surya.loginTest.model.Plant;
import com.surya.loginTest.model.Similarity;

public class BestMatching {
	private List<Double> mainImg;
	private List<Plant> plants;

	private Map<Double, Plant> matchingSet;

	public BestMatching(List<Double> mainImg, List<Plant> plants) {
		matchingSet = new HashMap<>();
		this.mainImg = mainImg;
		this.plants = plants;

		compute();
	}

	public List<Similarity> getMatchingValues() {
		List<Similarity> similar = new LinkedList<>();

		for (Map.Entry<Double, Plant> entry : matchingSet.entrySet()) {
			Similarity s = new Similarity();
			Plant plant = (Plant) entry.getValue();
			s.setPlant(plant);
			s.setSimilar(entry.getKey());
			similar.add(s);
		}
		return similar;
	}

	public void compute() {
		try {
			int i = 0;
			for (Plant plant : this.plants) {
				Double sim = new Double(calculateSimilarity(plant.getDistanceDifferenceFromMean()));
				System.out.println(plant.getCommonName() + " has similarity of " + sim + "%");
				matchingSet.put(sim, plant);
			}

			this.matchingSet = sortByComparator(matchingSet, false);
		} catch (NotSameSizeException e) {
			System.out.println(e.getMessage());
		}
	}

	private double calculateSimilarity(List<Double> mainImg1) throws NotSameSizeException {

		double sim = SimilarityFinder.calcluateSimilarity(this.mainImg, mainImg1);

		return sim;
	}

	private static Map<Double, Plant> sortByComparator(Map<Double, Plant> unsortMap, final boolean order) {

		List<Entry<Double, Plant>> list = new LinkedList<Entry<Double, Plant>>(unsortMap.entrySet());

		// Sorting the list based on values
		Collections.sort(list, new Comparator<Entry<Double, Plant>>() {
			public int compare(Entry<Double, Plant> o1, Entry<Double, Plant> o2) {
				if (order) {
					return o1.getKey().compareTo(o2.getKey());
				} else {
					return o2.getKey().compareTo(o1.getKey());

				}
			}
		});

		// Maintaining insertion order with the help of LinkedList
		Map<Double, Plant> sortedMap = new LinkedHashMap<Double, Plant>();
		for (Entry<Double, Plant> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}

}
