package com.surya.imgprocess.util;

import org.opencv.core.Scalar;

public class CommonColors {
	
	
	//COLOR FORMAT IS B_G_R
	public static Scalar BLUE=new Scalar(255,0,0);
	public static Scalar GREEN=new Scalar(0,255,0);
	public static Scalar RED=new Scalar(0,0,255);
	public static Scalar BLACK=new Scalar(0,0,0);
	public static Scalar WHITE=new Scalar(255,255,255);
}
