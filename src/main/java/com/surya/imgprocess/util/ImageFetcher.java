package com.surya.imgprocess.util;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class ImageFetcher {

	public Mat getMatrixFromImage(String path) throws FileNotFoundException {
		// Mat img=Imgcodecs.imread(path);
		// if(img==null) throw new FileNotFoundException("Image at "+path+" is not
		// found");
		// else System.out.println("Image loaded from "+path+" with row and col
		// "+img.rows()+"X"+img.cols());
		System.out.println("URL:" + path);
		BufferedImage image = null;
		try {
			image = ImageIO.read(new URL(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block

			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return bufferedImageToMat(image);
	}

	public static Mat bufferedImageToMat(BufferedImage bi) {
		Mat mat = new Mat(bi.getHeight(), bi.getWidth(), CvType.CV_8UC3);
		byte[] data = ((DataBufferByte) bi.getRaster().getDataBuffer()).getData();
		mat.put(0, 0, data);
		System.out.println("Image loaded  with row and col " + mat.rows() + "X" + mat.cols());
		return mat;
	}
}
