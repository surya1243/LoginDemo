$(window).load(function () {
	$("#container").html('<div class="blue bigger-150">Loading  <i class="ace-icon fa fa-spinner fa-spin blue bigger-150"></i>  </div>')
	console.log( "window loaded" );
	var container = $("#container");
	
	$.ajax({
		url: '/api/count/family',
	   	type: "GET",
	   	dataType: 'json',
	    contentType: 'application/json',   	
	   	success: function(response){
	   		console.log(response);
	   		
	   		console.log("in js file "+JSON.stringify(response));
	   		var processed_json = new Array();
	   		for (var key in response) {
	   		    if (response.hasOwnProperty(key)) {
	   		    	processed_json.push([key, response[key]])
	   		        console.log(key + " -> " + response[key]);
	   		    }
	   		}
	   		
	   		
	   		console.log(processed_json);	
	   		console.log(Object.keys(processed_json).length);
	   	 
	   		if(response){
	   			Highcharts.chart('container', {
	   			    chart: {
	   			        type: 'pie',
	   			        options3d: {
	   			            enabled: true,
	   			            alpha: 45,
	   			            beta: 0
	   			        }
	   			    },
	   			    title: {
	   			        text: 'Overview of Plant database by "Family"'
	   			    },
	   			    credits: {
	   			        enabled: false
	   			    },	   			    
	   			    tooltip: {
	   			        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	   			    },
	   			    plotOptions: {
	   			        pie: {
	   			            allowPointSelect: true,
	   			            cursor: 'pointer',
	   			            depth: 35,
	   			            dataLabels: {
	   			                enabled: true,
	   			                format: '{point.name}'
	   			            }
	   			        }
	   			    },
	   			    series: [{
	   			        type: 'pie',
	   			        name: 'Family',
	   			        data: processed_json
	   			    }]
	   			});
	   		}else{
				$("#container").html("<strong>Oops, an error occured fetching the remote leaf data.</strong>");
			}
	   		
	   		
	   		
	   	}
		
		
	});
	
});

