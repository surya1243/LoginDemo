
$(document).ready(function () {
    $("#input-file-events").change(function () {
        fire_ajax_submit();
    });
});

function fire_ajax_submit() {
	//clear previous result
	$("#postResultDiv").empty();
	$("#progressShow").empty();
	$("#progressShow").html('<div class="blue bigger-110"> Getting result <i class="ace-icon fa fa-spinner fa-spin blue bigger-125"></i>  </div>')
    // Get form
    var form = $('#plant-form')[0];
    var data = new FormData(form);
    $("#btnSubmit").prop("disabled", true);

    $.ajax({
        type: "POST",        
        enctype: 'multipart/form-data',
        url: 'https://leaf-brains.herokuapp.com/api/ai',
        crossDomain: true,
        data: data,        
        processData: false, //prevent jQuery from automatically transforming the data into a query string
        contentType: false,
        cache: false,
        timeout: 600000,
        success : function(result) {
        	console.log(result);
        	var matchPercent=Math.round(result.leaf);
        	var nonLeafMatch =100-matchPercent;
        	
        	console.log("leaf ="+matchPercent+" non leaf="+nonLeafMatch);
			if(result.status){
				$("#progressShow").html('<span class="label label-xlg label-pink arrowed-in-right arrowed-in">'+
						'Leaf status</span><div class="space-2"></div>'+
						'<div class="progress">'+
				'<div class="progress-bar progress-bar-success" style="width:'+matchPercent+'%;">'+matchPercent+'%</div>'+
				'<div class="progress-bar progress-bar-danger" style="width:'+nonLeafMatch+'%;">'+nonLeafMatch+'%</div>'+
			'</div>');  
				if(matchPercent>60)
					$("#btnSubmit").prop("disabled", false);
				else
					$("#postResultDiv").html("<strong>Leaf Match must cross 60% or above</strong>");
			
			
			}else{
				$("#postResultDiv").html("<strong>Error</strong>");
			}
			
		},
        error: function (e) {

            $("#result").text(e.responseText);
            console.log("ERROR : ", e);
            $("#btnSubmit").prop("disabled", true);

        }
    });

}